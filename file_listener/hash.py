from subprocess import run, PIPE, CalledProcessError
from typing import Dict
from pathlib import Path

from typeguard import typechecked


class FileDoesNotExistException(Exception):
    pass


class DirectoryDoesNotExistException(Exception):
    pass


class CannotBeHashedException(Exception):
    pass


@typechecked
def hash_file(filepath: Path) -> str:
    if not filepath.exists() or not filepath.is_file():
        raise FileDoesNotExistException(f"{filepath} does not exist")
    try:
        return run(['sha256sum', str(filepath)], stdout=PIPE,
                   check=True).stdout.decode().split(' ')[0]
    except CalledProcessError as exc:
        raise CannotBeHashedException(f"Unable to hash {filepath}: {exc}")


@typechecked
def hash_directory(dirpath: Path) -> Dict[Path, str]:
    if not dirpath.exists() or not dirpath.is_dir():
        raise DirectoryDoesNotExistException(f"{dirpath} does not exist")
    return {p: hash_file(p) for p in dirpath.rglob('*') if p.is_file()}


@typechecked
def hash_path(path: Path) -> Dict[Path, str]:
    if path.is_file():
        return {path: hash_file(path)}
    if path.is_dir():
        return hash_directory(path)
    raise CannotBeHashedException(f"Unable to hash {path}")
