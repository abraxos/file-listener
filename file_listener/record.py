from pathlib import Path
from typing import Tuple, Optional, Union, Any
try:
    import sqlite3
    from sqlite3 import Connection
    SQLITE = True
except ModuleNotFoundError:
    SQLITE = False

from typeguard import typechecked

from file_listener.hash import hash_file


@typechecked
def record_path(record_dir: Path, path: Path) -> Path:
    path = path if not path.is_absolute() else path.relative_to('/')
    return record_dir / path


class FileRecordStorage():
    CREATE_SQL = """
    CREATE TABLE IF NOT EXISTS file_records (
     file_path text PRIMARY KEY);
    """
    GET_SQL = "SELECT * FROM file_records WHERE file_path = ?;"
    INSERT_SQL = "REPLACE INTO file_records (file_path) VALUES (?);"

    @typechecked
    def __init__(self, db_location: Path, use_sqlite: bool = SQLITE) -> None:
        self.location = db_location
        self._use_sqlite = use_sqlite
        self._initalize_db()

    @typechecked
    def _connection(self) -> Any:  # Actually returns Connection
        if self._use_sqlite:
            conn = sqlite3.connect(str(self.location))
            conn.row_factory = sqlite3.Row
            return conn
        return None

    @typechecked
    def _initalize_db(self) -> None:
        if self._use_sqlite:
            with self._connection() as con:
                con.execute(self.CREATE_SQL)
        else:
            assert not self.location.is_file(), \
                "Given DB location must not be an existing file if we aren't"\
                " using SQLite"
            self.location.mkdir(exist_ok=True, parents=True)

    @typechecked
    def __contains__(self, filepath: Path) -> bool:
        if self._use_sqlite:
            with self._connection() as con:
                cur = con.cursor()
                cur.execute(self.GET_SQL, (str(filepath),))
                result = cur.fetchone()
                return result is not None
        else:
            return record_path(self.location, filepath).exists()

    @typechecked
    def insert(self, filepath: Path) -> None:
        if self._use_sqlite:
            with self._connection() as con:
                con.execute(self.INSERT_SQL, (str(filepath),))
        else:
            record = record_path(self.location, filepath)
            record.parent.mkdir(exist_ok=True, parents=True)
            record.touch()


class HashedRecordStorage():
    CREATE_SQL = """
    CREATE TABLE IF NOT EXISTS hash_records (
     file_path text PRIMARY KEY,
     hash text NOT NULL);
    """
    GET_SQL = "SELECT * FROM hash_records WHERE file_path = ?;"
    INSERT_SQL = "REPLACE INTO hash_records (file_path, hash) VALUES (?, ?);"

    @typechecked
    def __init__(self, db_location: Path, use_sqlite: bool = SQLITE) -> None:
        self.location = db_location
        self._use_sqlite = use_sqlite
        self._initalize_db()

    @typechecked
    def _connection(self) -> Any:  # Actually returns a Connection
        if self._use_sqlite:
            conn = sqlite3.connect(str(self.location))
            conn.row_factory = sqlite3.Row
            return conn
        return None

    @typechecked
    def _initalize_db(self) -> None:
        if self._use_sqlite:
            with self._connection() as con:
                con.execute(self.CREATE_SQL)
        else:
            assert not self.location.is_file(), \
                "Given DB location must not be an existing file if we aren't"\
                " using SQLite"
            self.location.mkdir(exist_ok=True, parents=True)

    @typechecked
    def __contains__(self, filepath: Union[Tuple[Path, str], Path]) -> bool:
        if isinstance(filepath, tuple):
            path, file_hash = filepath
        else:
            path, file_hash = filepath, hash_file(filepath)
        if self._use_sqlite:
            with self._connection() as con:
                cur = con.cursor()
                cur.execute(self.GET_SQL, (str(path),))
                result = cur.fetchone()
                if result is not None:
                    return bool(result['hash'] == file_hash)
                return False
        else:
            record = record_path(self.location, path)
            if record.exists():
                return bool(record.open().read().strip() == file_hash)
            return False

    @typechecked
    def insert(self, filepath: Path, file_hash: Optional[str] = None) -> None:
        file_hash = file_hash if file_hash is not None else hash_file(filepath)
        if self._use_sqlite:
            with self._connection() as con:
                con.execute(self.INSERT_SQL, (str(filepath), file_hash))
        else:
            record = record_path(self.location, filepath)
            record.parent.mkdir(exist_ok=True, parents=True)
            record.touch()
            record.open('w').write(file_hash)
