from tempfile import TemporaryDirectory
from pathlib import Path

from typeguard import typechecked

from file_listener.hash import hash_path

from test_basics import deep_touch


EMPTY_HASH = 'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855'


@typechecked
def test_hashing_file() -> None:
    with TemporaryDirectory() as dirname:
        path = Path(dirname) / 'stuff.txt'
        deep_touch(path)
        assert hash_path(path) == {
            path: EMPTY_HASH}


@typechecked
def test_hashing_directory() -> None:
    with TemporaryDirectory() as dirname:
        root = Path(dirname)
        deep_touch(root / 'stuff.txt')
        deep_touch(root / 'stuff1.txt')
        deep_touch(root / 'more stuff' / 'stuff2.txt')
        deep_touch(root / 'more stuff' / 'stuff2.txt')

        assert hash_path(root) == {
            root / 'stuff.txt': EMPTY_HASH,
            root / 'stuff1.txt': EMPTY_HASH,
            root / 'more stuff' / 'stuff2.txt': EMPTY_HASH,
            root / 'more stuff' / 'stuff2.txt': EMPTY_HASH
        }
