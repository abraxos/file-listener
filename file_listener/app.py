"""A set of utilities for making applications that monitor directories"""
from typing import Callable, List, Dict, Set, Any, Union, Generator
from pathlib import Path
from traceback import print_exc
import os

import gevent
from gevent import Greenlet, sleep
from gevent.queue import Queue
from typeguard import typechecked
import gevent_inotifyx as inotify

from file_listener.record import FileRecordStorage, HashedRecordStorage


INCallback = Callable[[Path], Any]
Greenlets = List[Greenlet]
Records = Union[FileRecordStorage, HashedRecordStorage, None]


@typechecked
def _files(dir_path: Path) -> Generator[Path, None, None]:
    return (p for p in dir_path.rglob('*') if p.is_file())


class FileListenerApp(Greenlet):
    @typechecked
    def __init__(self, num_workers: int):
        super().__init__()
        self._tasks = Queue()
        self._running: Set[Path] = set()
        self._callbacks: Dict[Path, INCallback] = {}
        self._watchers: Greenlets = []
        self._workers: Greenlets = []
        self._records: Records = None
        self.num_workers = num_workers

    @typechecked
    def _enqueue(self, path: Path) -> None:
        if path.parent in self._callbacks:
            self._tasks.put_nowait((path, self._callbacks[path.parent]))

    @typechecked
    def keep_track_with(self, records: Records) -> 'FileListenerApp':
        self._records = records
        return self

    @typechecked
    def register(self, path: Path, func: INCallback) -> 'FileListenerApp':
        self._callbacks[path] = func
        return self

    @typechecked
    def _recorded(self, callback: INCallback, path: Path) -> None:
        if self._records is not None:
            if path.is_dir():
                if any(p not in self._records for p in _files(path)):
                    callback(path)
                    for child_file in _files(path):
                        self._records.insert(child_file)
            if path.is_file() and path not in self._records:
                callback(path)
                self._records.insert(path)

    @typechecked
    def _process_queue_task(self) -> None:
        while 42:
            path, callback = self._tasks.get()
            if path not in self._running:
                try:
                    self._running.add(path)
                    try:
                        if self._records is None:
                            callback(path)
                        else:
                            self._recorded(callback, path)
                    except Exception:  # pylint: disable=W0703
                        print_exc()
                finally:
                    self._running.remove(path)

    @typechecked
    def _watch_dir(self, dir_path: Path) -> None:
        descriptor = inotify.init()
        try:
            inotify.add_watch(descriptor, str(dir_path), inotify.IN_ALL_EVENTS)
            while 42:
                for event in inotify.get_events(descriptor):
                    if event.name and ((inotify.IN_CLOSE_WRITE |
                                        inotify.IN_ATTRIB | inotify.IN_MOVED_TO)
                                       & event.mask):
                        self._enqueue(dir_path / event.name)
        finally:
            os.close(descriptor)

    @typechecked
    def _spawn_watchers(self) -> Greenlets:
        return [gevent.spawn(self._watch_dir, d) for d in
                self._callbacks]

    @typechecked
    def _spawn_workers(self) -> Greenlets:
        return [gevent.spawn(self._process_queue_task) for _ in
                range(self.num_workers)]

    @typechecked
    def run(self) -> None:
        for watched_path in self._callbacks:
            for path in watched_path.iterdir():
                self._enqueue(path)
        gevent.joinall(self._spawn_workers() + self._spawn_watchers())

    @typechecked
    def __enter__(self) -> 'FileListenerApp':
        self.start()
        sleep(0.1)
        return self

    @typechecked
    def __exit__(self, _: Any, __: Any, ___: Any) -> None:
        self.kill()
        sleep(0.1)
