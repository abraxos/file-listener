from tempfile import NamedTemporaryFile, TemporaryDirectory
from pathlib import Path

from typeguard import typechecked

from file_listener.record import FileRecordStorage, HashedRecordStorage

from test_hashing import EMPTY_HASH


@typechecked
def test_hashed_record_insertion() -> None:
    with NamedTemporaryFile(suffix='.db') as db_file:
        records = HashedRecordStorage(Path(db_file.name))
        records.insert(Path('/test/stuff.txt'), EMPTY_HASH)
        assert (Path('/test/stuff.txt'), EMPTY_HASH) in records
        assert (Path('stuff'), EMPTY_HASH) not in records


@typechecked
def test_file_record_insertion() -> None:
    with NamedTemporaryFile(suffix='.db') as db_file:
        records = FileRecordStorage(Path(db_file.name))
        records.insert(Path('/test/stuff.txt'))
        assert Path('/test/stuff.txt') in records
        assert Path('stuff') not in records


@typechecked
def test_hashed_record_insertion_without_sqlite() -> None:
    with TemporaryDirectory() as dirname:
        db_path = Path(dirname) / 'records.db'
        records = HashedRecordStorage(Path(db_path), use_sqlite=False)
        records.insert(Path('/test/stuff.txt'), EMPTY_HASH)
        assert (Path('/test/stuff.txt'), EMPTY_HASH) in records
        assert (Path('stuff'), EMPTY_HASH) not in records


@typechecked
def test_file_record_insertion_without_sqlite() -> None:
    with TemporaryDirectory() as dirname:
        db_path = Path(dirname) / 'records.db'
        records = FileRecordStorage(Path(db_path), use_sqlite=False)
        records.insert(Path('/test/stuff.txt'))
        assert Path('/test/stuff.txt') in records
        assert Path('stuff') not in records
