# File Listener Python Module

This is a small module I wrote when I discovered that several different applications of mine followed the same pattern, i.e. they needed to scan a directory, or set of directories for files, do something to those files, and then keep listening for new files. The architypal example is an rsync job where we want to rsync any file added to a directory, as soon as its added.

This module facilitates that by allowing you to register directories and callbacks to be executed on every file in those directories both for existing files and for those added after. It also gives us a few features that are harder if we just scripted with inotifytools, for example, we can set numbers of workers thereby limiting the number of tasks running at any one time, but still execute all of the tasks.

## Installation

Simply clone the repository and then install with:

```
pip install file-listener/
```

## Usage

### Basic example:

```
from pathlib import Path
from file_listener import FileListenerApp

def callback(path: Path) -> None:
    print(f'{path} has been added!')

listener = FileListenerApp(10).register('/path/to/my/dir', callback)
listener.start()
...
listener.kill()
```

This example will print out which files have been added to a directory before and while the listener was running

### Using SQLite/sha256sum for Tracking Files

The file-listener can use a SQLite database to keep track of the files that have been processed before. There are two separate mechanisms for managing this. Both approaches require that the file listener have access to a read/write-able SQLite database (which it creates itself given a path).

+ With file-names and hashing:
  + This approach uses `sha256sum` on every single file when an event is about to be processed, which means that `sha256sum` needs to be installed on the machine in question.
  + Every single file is checked for in the database to see whether it has been processed before. If the file is not in the database, the callback will be fired immediately, otherwise the file will be hashed to verify that the current hash matches the pre-existing one (i.e. that the file has not been changed). If the hashes are the same, the callback will not be executed, otherwise it will.
  + When a callback is complete, the file will be hashed and stored in the database.
  + When a directory is altered (as opposed to a file), the entire directory will be hashed, file-by-file, and checked.
+ Only with file-names:
  + This approach is similar to the one with hashing in that files that have already been processed will not be processed anymore, however, it does not use hashes to check for changes, it only uses file names to determine whether the files need to be processed again.
  + When a directory is altered (as opposed to a file), the entire directory will be checked and if a new file was added that was not there before, the callback will be executed.
  + The idea is that, of course, this approach is much faster for larger files, but it is unable to pick up on changes that may have been made while the application was not running.

#### Example With Hashing:

```python
from pathlib import Path
from file_listener import FileListenerApp
from file_listener.record import HashedRecordStorage


def callback(path: Path) -> None:
    print(f'{path} has been added!')


listener = FileListenerApp(10)\
        .register('/path/to/my/dir', callback)\
        .keep_track_with(HashedRecordStorage('my.db'))
listener.start()
...
listener.kill()
```

#### Example Without Hashing (just file-names):

```python
from pathlib import Path
from file_listener import FileListenerApp
from file_listener.record import FileRecordStorage


def callback(path: Path) -> None:
    print(f'{path} has been added!')


listener = FileListenerApp(10)\
        .register('/path/to/my/dir', callback)\
        .keep_track_with(FileRecordStorage('my.db'))
listener.start()
...
listener.kill()
```