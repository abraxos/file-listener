"""Setup file for delivery-service"""
from setuptools import setup, find_packages


TEST_REQUIREMENTS = ['pytest', 'pytest-cov', 'pylint', 'mypy', 'flake8']

with open('README.md') as f:
    README = f.read()

with open('LICENSE') as f:
    LICENSE = f.read()

with open('requirements.txt') as f:
    REQUIREMENTS = f.readlines()

setup(
    name='file-listener',
    version='0.2.0',
    description='A module providing utilities for applications that respond '
                'to file system events. A wrapper over gevent/inotify.',
    long_description=README,
    author='Eugene Kovalev',
    author_email='eugene@kovalev.systems',
    url='https://gitlab.com/abraxos/file-listener',
    license=LICENSE,
    packages=find_packages(exclude=('tests', 'docs')),
    install_requires=REQUIREMENTS + TEST_REQUIREMENTS,
)
