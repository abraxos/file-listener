from pathlib import Path
from tempfile import TemporaryDirectory
from typing import List
from shutil import move

from typeguard import typechecked
from gevent import sleep

from file_listener import FileListenerApp
from file_listener.record import FileRecordStorage, HashedRecordStorage


@typechecked
def deep_touch(path: Path) -> None:
    if not path.exists():
        path.parent.mkdir(parents=True, exist_ok=True)
        path.touch()


def test_import() -> None:
    assert FileListenerApp.__name__ == 'FileListenerApp'


def test_react_to_new_file_in_one_watched_directory() -> None:
    with TemporaryDirectory() as dirname:
        files_touched: List[Path] = []

        def basic_callback(path: Path) -> None:
            print("Callback called!")
            files_touched.append(path)

        dir_path = Path(dirname)
        with FileListenerApp(1).register(dir_path, basic_callback):
            deep_touch(dir_path / 'stuff')
        assert dir_path / 'stuff' in files_touched


def test_react_to_new_file_and_existing_file() -> None:
    with TemporaryDirectory() as dirname:
        files_touched: List[Path] = []

        def basic_callback(path: Path) -> None:
            print("Callback called!")
            files_touched.append(path)

        dir_path = Path(dirname)
        deep_touch(dir_path / 'existing')
        with FileListenerApp(1).register(dir_path, basic_callback):
            deep_touch(dir_path / 'new')
        assert dir_path / 'existing' == files_touched[0]
        assert dir_path / 'new' == files_touched[1]


def test_reacting_to_files_without_record_keeping() -> None:
    with TemporaryDirectory() as dirname:
        files_touched: List[Path] = []
        watched_dir = Path(dirname) / 'watch'
        watched_dir.mkdir(exist_ok=True, parents=True)

        def basic_callback(path: Path) -> None:
            print("Callback called!")
            files_touched.append(path)

        with FileListenerApp(1).register(watched_dir, basic_callback):
            stuff = watched_dir / 'stuff'
            deep_touch(stuff)
            stuff.unlink()
            deep_touch(stuff)
        assert sum(1 for e in files_touched if e == stuff) == 2


def test_reacting_to_files_with_record_keeping() -> None:
    with TemporaryDirectory() as dirname:
        files_touched: List[Path] = []
        watched_dir = Path(dirname) / 'watch'
        records_db = Path(dirname) / 'test.db'
        watched_dir.mkdir(exist_ok=True, parents=True)
        something = Path(dirname) / 'something'
        something.mkdir()

        def basic_callback(path: Path) -> None:
            print("Callback called!")
            files_touched.append(path)

        records = FileRecordStorage(records_db)
        with FileListenerApp(1)\
                .register(watched_dir, basic_callback)\
                .keep_track_with(records):
            deep_touch(something / '1')
            deep_touch(something / '2')
            deep_touch(something / '3')
            deep_touch(something / '4')

            stuff = watched_dir / 'stuff'
            deep_touch(stuff)
            stuff.unlink()
            deep_touch(stuff)
            move(str(something), str(watched_dir))
        assert sum(1 for e in files_touched if e == stuff) == 1
        assert watched_dir / something.name in files_touched
        assert watched_dir / something.name / '1' in records
        assert watched_dir / something.name / '2' in records
        assert watched_dir / something.name / '3' in records
        assert watched_dir / something.name / '4' in records


def test_reacting_to_files_with_hash_record_keeping() -> None:
    with TemporaryDirectory() as dirname:
        files_touched: List[Path] = []
        watched_dir = Path(dirname) / 'watch'
        records_db = Path(dirname) / 'test.db'
        watched_dir.mkdir(exist_ok=True, parents=True)
        something = Path(dirname) / 'something'
        something.mkdir()

        def basic_callback(path: Path) -> None:
            print("Callback called!")
            files_touched.append(path)

        records = HashedRecordStorage(records_db)
        with FileListenerApp(1)\
                .register(watched_dir, basic_callback)\
                .keep_track_with(records):
            deep_touch(something / '1')
            deep_touch(something / '2')
            deep_touch(something / '3')
            deep_touch(something / '4')

            stuff = watched_dir / 'stuff'
            deep_touch(stuff)
            sleep(0.01)
            assert sum(1 for e in files_touched if e == stuff) == 1
            stuff.unlink()
            sleep(0.01)
            assert sum(1 for e in files_touched if e == stuff) == 1
            deep_touch(stuff)
            sleep(0.01)
            assert sum(1 for e in files_touched if e == stuff) == 1
            stuff.unlink()
            sleep(0.01)
            assert sum(1 for e in files_touched if e == stuff) == 1
            deep_touch(stuff)
            stuff.open('w').write('something')
            sleep(0.01)
            assert sum(1 for e in files_touched if e == stuff) == 2
            stuff.unlink()
            stuff.open('w').write('something')
            sleep(0.01)
            assert sum(1 for e in files_touched if e == stuff) == 2
            stuff.unlink()
            deep_touch(stuff)
            sleep(0.01)
            assert sum(1 for e in files_touched if e == stuff) == 3
            stuff.unlink()
            stuff.open('w').write('something else')
            sleep(0.01)
            assert sum(1 for e in files_touched if e == stuff) == 4
            move(str(something), str(watched_dir))
        assert sum(1 for e in files_touched if e == stuff) == 4
        assert watched_dir / something.name in files_touched
        assert watched_dir / something.name / '1' in records
        assert watched_dir / something.name / '2' in records
        assert watched_dir / something.name / '3' in records
        assert watched_dir / something.name / '4' in records
